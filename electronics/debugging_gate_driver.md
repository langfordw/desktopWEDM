### Debugging Gate Drivers

It seems like the bootstrap portion of the gate drivers is not working.

I disconnected the gate drivers from the mosfets and noticed that they wouldn't boost over ~12V (which is their supply voltage).

With a 100ohm resistor as the load and 20V applied to V+, these are the curves I'm seeing (with ~15mA average current draw from the supply):

Pulse output:

<img src="images/100ohm_pulse.png">

The gate of the bottom discharge FET:

<img src="images/100ohm_bottom_gate.png">

The bootstrap pin of the discharge gate driver:

<img src="images/100ohm_bottom_hb.png">

The drain of the discharge FET:

<img src="images/100ohm_bottom_source.png">

The bootstrap pin of the charge gate driver:

<img src="images/100ohm_top_hb.png">

I figured this out… I overlooked the fact that this half-bridge driver doesn't include a charge pump and relies on the HS pin (between the low and high mosfet being grounded routinely to charge):

> With the VHB capacitor connected to HB and the HS pins, the VHB capacitor charge is refreshed every switching cycle *when HS transitions to ground*.