### Datasheets:

[complete](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8331-8-and-16-bit-AVR-Microcontroller-XMEGA-AU_Manual.pdf)

[128a4u specific](http://ww1.microchip.com/downloads/en/DeviceDoc/Atmel-8387-8-and16-bit-AVR-Microcontroller-XMEGA-A4U_Datasheet.pdf)

### Pins

#### My Pins:

<img src="../images/xmega_pinout.png">

<img src="../images/pinMapping.png">

<img src="../images/portA.png" width="700px">

<img src="../images/portB.png" width="700px">

<img src="../images/portC.png" width="700px">

<img src="../images/portD.png" width="700px">

<img src="../images/portE.png" width="700px">

<img src="../images/portR.png" width="700px">

<img src="../images/usart_registers.png" width="700px">