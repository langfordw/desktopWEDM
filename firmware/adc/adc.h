#define ADCACAL0_offset 0x20
#define ADCACAL1_offset 0x21
#define ADCBCAL0_offset 0x24
#define ADCBCAL1_offset 0x25

#define ADC_CH_CHIF_bm  0x01  /* Channel Interrupt Flag bit mask. */


#define ADC_ConvMode_and_Resolution_Config(_adc, _signedMode, _resolution)     \
	((_adc)->CTRLB = ((_adc)->CTRLB & (~(ADC_RESOLUTION_gm|ADC_CONMODE_bm)))|  \
		(_resolution| ( _signedMode? ADC_CONMODE_bm : 0)))
#define ADC_Reference_Config(_adc, _convRef)                                   \
	((_adc)->REFCTRL = ((_adc)->REFCTRL & ~(ADC_REFSEL_gm)) | _convRef)
#define ADC_Prescaler_Config(_adc, _div)                                       \
	((_adc)->PRESCALER = ((_adc)->PRESCALER & (~ADC_PRESCALER_gm)) | _div)
#define ADC_Ch_InputMode_and_Gain_Config(_adc_ch, _inputMode, _gain)           \
	(_adc_ch)->CTRL = ((_adc_ch)->CTRL &                                   \
	                  (~(ADC_CH_INPUTMODE_gm|ADC_CH_GAIN_gm))) |        \
	                  ((uint8_t) _inputMode|_gain)
#define ADC_Ch_InputMux_Config(_adc_ch, _posInput, _negInput)                  \
	((_adc_ch)->MUXCTRL = (uint8_t) _posInput | _negInput)
#define ADC_Enable(_adc) ((_adc)->CTRLA |= ADC_ENABLE_bm)
#define COMMON_MODE_CYCLES 16

#define ADC_Conversions_Start(_adc, _channelMask)                         \
	(_adc)->CTRLA |= _channelMask &                                   \
	              (ADC_CH0START_bm | ADC_CH1START_bm |                \
	               ADC_CH2START_bm | ADC_CH3START_bm)
#define ADC_Ch_Conversion_Start(_adc_ch) ((_adc_ch)->CTRL |= ADC_CH_START_bm)
#define ADC_Ch_Conversion_Complete(_adc_ch)                                    \
	(((_adc_ch)->INTFLAGS & ADC_CH_CHIF_bm) != 0x00)


#define ADC_BandgapReference_Enable(_adc) ((_adc)->REFCTRL |= ADC_BANDGAP_bm)
#define ADC_BandgapReference_Disable(_adc) ((_adc)->REFCTRL &= ~ADC_BANDGAP_bm)


uint8_t SP_ReadCalibrationByte( uint8_t index )
{
	uint8_t result;
	/* Load the NVM Command register to read the calibration row. */
	NVM_CMD = NVM_CMD_READ_CALIB_ROW_gc;
	result = pgm_read_byte(index);
	/* Clean up NVM Command register. */
	NVM_CMD = NVM_CMD_NO_OPERATION_gc;
	return result;
}
void ADC_Wait_8MHz(ADC_t * adc)
{
	/* Store old prescaler value. */
	uint8_t prescaler_val = adc->PRESCALER;

	/* Set prescaler value to minimum value. */
	adc->PRESCALER = ADC_PRESCALER_DIV4_gc;

	/* Wait 4*COMMON_MODE_CYCLES for common mode to settle. */
	_delay_us(4*COMMON_MODE_CYCLES);

	/* Set prescaler to old value*/
	adc->PRESCALER = prescaler_val;
}

int16_t ADC_ResultCh_GetWord_Signed(ADC_CH_t * adc_ch, int8_t signedOffset)
{
	int16_t answer;

	/* Clear interrupt flag.*/
	adc_ch->INTFLAGS = ADC_CH_CHIF_bm;

	/* Return result register contents*/
	answer = adc_ch->RES - signedOffset;

	return answer;
}
uint16_t ADC_ResultCh_GetWord(ADC_CH_t * adc_ch)
{
	/* Clear interrupt flag.*/
	adc_ch->INTFLAGS = ADC_CH_CHIF_bm;

	/* Return result register contents*/
	return adc_ch->RES;;
}

int8_t ADC_Offset_Get_Signed(ADC_t * adc, ADC_CH_t *ch, int oversampling_n)
{
	if (oversampling_n) {
		int16_t offset=0;
		for (int i=0; i<oversampling_n; i++) {
			/* Do one conversion to find offset. */
			ADC_Ch_Conversion_Start(ch);

			do{
			} while (!ADC_Ch_Conversion_Complete(ch));
			offset += ADC_ResultCh_GetWord_Signed(ch, 0x00);
		}
		return ((int8_t)(offset/oversampling_n));
	} else {
		int8_t offset=0;

		/* Do one conversion to find offset. */
		ADC_Ch_Conversion_Start(ch);

		do{
		} while (!ADC_Ch_Conversion_Complete(ch));
		offset = (uint8_t)ADC_ResultCh_GetWord_Signed(ch, 0x00);

		return offset;
	}
}