//
// usart example
// output the value of a counter and echo back any serial messages received
//
// wkl 2018
//
#include <stdio.h>
#include <avr/interrupt.h>
#include "serial.h"
#include "adc.h"

#define serialPort PORTC
#define txPin PIN3_bm
#define rxPin PIN2_bm

#define ledPort PORTC
#define ledPin PIN1_bm

#define chargePort PORTC
#define chPin PIN6_bm //charge
#define dchPin PIN7_bm //discharge

#define maxBufferSize 32

static char output_buffer[maxBufferSize+1] = {0};
static char input_buffer[maxBufferSize+1] = {0};

int position_in_buffer = 0;
uint16_t last_tx_rtc = 0;
int counter = 0;
int inc = 1;
uint16_t ADC_result = 0;
uint16_t offset = 0;


USART_data_t USART_data;

// send back the data
void send_packet(char* buffer, uint8_t len){
	for (uint8_t i=0; i <= len; i++) {
		usart_send_byte(&USART_data,buffer[i]);
	}
	// toggle LED for every packet sent
	// ledPort.OUT ^= ledPin;
}

// read available data and send it back
void echo_usart(){
	if (USART_RXBufferData_Available(&USART_data)) {
		input_buffer[position_in_buffer] = USART_RXBuffer_GetByte(&USART_data);
		if (input_buffer[position_in_buffer] == '\n') {
			send_packet(input_buffer,position_in_buffer);
			position_in_buffer = 0;
		}
		else{
			position_in_buffer++;
		}
	}
}

// function to write data to buffer
void writeToBuffer(char *buff, char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vsnprintf(buff, maxBufferSize, fmt, args);
    va_end(args);
}

void set_pwm(uint16_t duty){
	TCC0.CCDBUF = duty; //set compare value
	do {} while(TCC0.INTFLAGS && TC0_OVFIF_bm == 0 );  //wait
	TCC0.INTFLAGS = TC0_OVFIF_bm;
}

// interrupt service routine for RX
ISR(USARTC0_RXC_vect){
	USART_RXComplete(&USART_data);
	echo_usart();

}

// interrupt service routine for data transmit buffer empty
ISR(USARTC0_DRE_vect){
	USART_DataRegEmpty(&USART_data);
}

// ISR(RTC_OVF_vect) {
// 	counter++;
// }

int main(void) {
	// set up clock
	OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
	while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

	// disable interrupts
	cli();

	//setup real time counter
	CLK.RTCCTRL	= CLK_RTCSRC_RCOSC_gc|CLK_RTCEN_bm;
	RTC.CTRL |= RTC_PRESCALER_DIV1_gc;

	// configure pins
	ledPort.DIRSET = ledPin; //led
	serialPort.OUTSET = txPin; //tx
	serialPort.DIRSET = txPin; //tx
	serialPort.DIRCLR = rxPin; //rx
	chargePort.DIRSET = chPin | dchPin; // charge and discharge
	chargePort.PIN6CTRL |= PORT_OPC_PULLDOWN_gc; //turn on pull-down resistors (when in input mode)
	chargePort.PIN7CTRL |= PORT_OPC_PULLDOWN_gc; //turn on pull-down resistors (when in input mode)
	chargePort.REMAP |= PORT_TC0D_bm | PORT_TC0C_bm;//0b00001100; // remap OCOC and OCOD to our charge and discharge pins

	// setup USART
	USART_InterruptDriver_Initialize(&USART_data, &USARTC0, USART_DREINTLVL_LO_gc);
	USART_Format_Set(USART_data.usart, USART_CHSIZE_8BIT_gc,
                     USART_PMODE_DISABLED_gc, 0);
	USART_RxdInterruptLevel_Set(USART_data.usart, USART_RXCINTLVL_LO_gc);
	//take f_sysclk/(BSEL+1) ~= f_baud*16 with zero scale.  See manual or spreadsheet for scale defs
	USART_Baudrate_Set(&USARTC0, 131 , -3); //11520 baud with -.1% error
	USART_Rx_Enable(USART_data.usart);
	USART_Tx_Enable(USART_data.usart);

	// setup pwm
	TCC0.PER = 0x0400; //set up 1024 resolution
	TCC0.CTRLB |= TC_WGMODE_SS_gc;//( TCC0.CTRLB & ~TC0_WGMODE_gm ) | TC_WGMODE_SS_gc; //single slope
	TCC0.CTRLA |= TC_CLKSEL_DIV256_gc; //TC_CLKSEL_DIV64_gc;  //set clock divider
	set_pwm(900);

	// setup AWEX
	AWEXC.CTRL |= AWEX_DTICCDEN_bm | AWEX_DTICCCEN_bm;; // enable DTI override on C and D channel (PC6 and PC7)
	AWEXC.STATUS &= ~AWEX_FDF_bm; // clear fault detection flag
	AWEXC.OUTOVEN |= chPin | dchPin; // enable output overide on charge and discharge pins
	AWEXC.DTBOTH = 32; // set deadtime (measured in clock cyles)

	// setup ADC

	//calibrate ADC to reduce nonlinearity
	ADCA.CALL = SP_ReadCalibrationByte( PROD_SIGNATURES_START + ADCACAL0_offset );
	ADCA.CALH = SP_ReadCalibrationByte( PROD_SIGNATURES_START + ADCACAL1_offset );
	ADC_ConvMode_and_Resolution_Config(&ADCA, 0, ADC_RESOLUTION_12BIT_gc); // unsigned, 12 bit
	// ADC_BandgapReference_Enable(&ADCA); //1V bandgap by default
	ADC_Reference_Config(&ADCA, ADC_REFSEL_INTVCC_gc);
	//_delay_ms(200);
	// ADC_Prescaler_Config(&ADCA, ADC_PRESCALER_DIV32_gc);
	ADC_Prescaler_Config(&ADCA, ADC_PRESCALER_DIV16_gc); //speed test
	// ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH0,
	//                                  ADC_CH_INPUTMODE_DIFFWGAINL_gc,
	//                                  ADC_CH_GAIN_16X_gc);
	ADC_Ch_InputMode_and_Gain_Config(&ADCA.CH0,
	                                 ADC_CH_INPUTMODE_SINGLEENDED_gc,
	                                 ADC_CH_GAIN_1X_gc);

	//ADC_Ch_InputMux_Config(&ADCA.CH0, ADC_CH_MUXPOS_PIN6_gc, ADC_CH_MUXNEGL_PIN3_gc);
	ADC_Ch_InputMux_Config(&ADCA.CH0, ADC_CH_MUXPOS_PIN1_gc, ADC_CH_MUXNEG_PIN1_gc);
	// ADCA.CH0.INTCTRL = ADC_CH_INTMODE_COMPLETE_gc | ADC_CH_INTLVL_HI_gc;

	ADC_Enable(&ADCA);
	ADC_Wait_8MHz(&ADCA);	
	offset = ADC_Offset_Get_Signed(&ADCA, &(ADCA.CH0), 100);

	
	// enable interrupts
	PMIC.CTRL |= PMIC_LOLVLEX_bm;
	sei();
	
	while (1) {
		if (RTC.CNT - last_tx_rtc > 2){
			ADC_Ch_Conversion_Start(&ADCA.CH0);
			while(!ADC_Ch_Conversion_Complete(&ADCA.CH0)){};
			ADC_result = ADC_ResultCh_GetWord_Signed(&ADCA.CH0,offset);
			writeToBuffer(&output_buffer[0],"%d\n", ADC_result); 
			send_packet(output_buffer,maxBufferSize); 
			last_tx_rtc = RTC.CNT;
		}
	}
}


