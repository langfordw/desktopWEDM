#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#define serialPort PORTC
#define txPin PIN3_bm
#define rxPin PIN2_bm
#define ledPort PORTC
#define ledPin PIN1_bm

int main(void) {
    // set up clock
    OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
    while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
    CCP = CCP_IOREG_gc; // enable protected register change
    CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

    ledPort.DIRSET = ledPin;

    serialPort.OUTSET = txPin;
    serialPort.DIRSET = txPin;
    serialPort.DIRCLR = rxPin;

    //set baudrate and frame format
    // 115200 with -0.1% error
    uint16_t bsel = 131;
    USARTC0.BAUDCTRLA = ((uint8_t)bsel); // lower 8 bits of 12 bit baud
    USARTC0.BAUDCTRLB = (-3 << USART_BSCALE0_bp)|(bsel >> 8); // upper 4 bits of 12 bit baud

    //set mode of operation
    // by default CMODE is asynchronoous USART
    USARTC0.CTRLC = ((uint8_t) USART_CHSIZE_8BIT_gc) | (USART_PMODE_DISABLED_gc);

    //enable tx or rx
    USARTC0.CTRLB |= USART_TXEN_bm;

    while (1) {
        ledPort.OUT ^= ledPin;
        USARTC0.STATUS |= USART_DREIF_bm;
        USARTC0.DATA = 'h';
        while (!(USARTC0.STATUS&USART_TXCIF_bm)) { }
        USARTC0.STATUS |= USART_DREIF_bm;
        USARTC0.DATA = 'i';
        while (!(USARTC0.STATUS&USART_TXCIF_bm)) { }
        USARTC0.STATUS |= USART_DREIF_bm;
        USARTC0.DATA = '\n';
        while (!(USARTC0.STATUS&USART_TXCIF_bm)) { }
        _delay_ms(100);
    }
}