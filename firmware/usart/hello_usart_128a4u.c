//
// usart example
// output the value of a counter and echo back any serial messages received
//
// wkl 2018
//
#include <stdio.h>
#include <avr/interrupt.h>
#include "serial.h"

#define serialPort PORTC
#define txPin PIN3_bm
#define rxPin PIN2_bm

#define ledPort PORTC
#define ledPin PIN1_bm

#define maxBufferSize 32

static char output_buffer[maxBufferSize+1] = {0};
static char input_buffer[maxBufferSize+1] = {0};

int position_in_buffer = 0;
uint16_t last_tx_rtc = 0;
uint32_t counter = 0;

USART_data_t USART_data;

// send back the data
void send_packet(char* buffer, uint8_t len){
	for (uint8_t i=0; i <= len; i++) {
		usart_send_byte(&USART_data,buffer[i]);
	}
	// toggle LED for every packet sent
	ledPort.OUT ^= ledPin;
}

// read available data and send it back
void echo_usart(){
	if (USART_RXBufferData_Available(&USART_data)) {
		input_buffer[position_in_buffer] = USART_RXBuffer_GetByte(&USART_data);
		if (input_buffer[position_in_buffer] == '\n') {
			send_packet(input_buffer,position_in_buffer);
			position_in_buffer = 0;
		}
		else{
			position_in_buffer++;
		}
	}
}

// function to write data to buffer
void writeToBuffer(char *buff, char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vsnprintf(buff, maxBufferSize, fmt, args);
    va_end(args);
}

// interrupt service routine for RX
ISR(USARTC0_RXC_vect){
	USART_RXComplete(&USART_data);
	echo_usart();

}

// interrupt service routine for data transmit buffer empty
ISR(USARTC0_DRE_vect){
	USART_DataRegEmpty(&USART_data);
}

int main(void) {
	// set up clock
	OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
	while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

	// configure pins
	ledPort.DIRSET = ledPin; //led
	PORTC.OUTSET = txPin; //tx
	PORTC.DIRSET = txPin; //tx
	PORTC.DIRCLR = rxPin; //rx

	// setup USART
	USART_InterruptDriver_Initialize(&USART_data, &USARTC0, USART_DREINTLVL_LO_gc);
	USART_Format_Set(USART_data.usart, USART_CHSIZE_8BIT_gc,
                     USART_PMODE_DISABLED_gc, 0);
	USART_RxdInterruptLevel_Set(USART_data.usart, USART_RXCINTLVL_LO_gc);
	//take f_sysclk/(BSEL+1) ~= f_baud*16 with zero scale.  See manual or spreadsheet for scale defs
	USART_Baudrate_Set(&USARTC0, 131 , -3); //11520 baud with -.1% error
	USART_Rx_Enable(USART_data.usart);
	USART_Tx_Enable(USART_data.usart);
	
	//enable interrupts
	PMIC.CTRL |= PMIC_LOLVLEX_bm;
	sei();
	
	while (1) {
		writeToBuffer(&output_buffer[0],"count=%d\n", counter++); 
		send_packet(output_buffer,maxBufferSize); 
		_delay_ms(100);	
	}
}


