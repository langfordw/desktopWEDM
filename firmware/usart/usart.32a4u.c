//
// usart example
//
// sec 2016
//
#include <stdio.h>
#include <avr/sleep.h>
#include <avr/interrupt.h>
#include "serial.h"

#define output(directions,pin) (directions |= pin) // set port direction for output
#define input(directions,pin) (directions &= (~pin)) // set port direction for input
#define set(port,pin) (port |= pin) // set port pin
#define clear(port,pin) (port &= (~pin)) // clear port pin


static char output_buffer[100] = {0};
static char input_buffer[100] = {0};

int position_in_buffer=0;
uint16_t last_tx_rtc = 0;

USART_data_t USART_data;

uint16_t pwm_duty = 0;

void set_pwm(uint16_t duty){
	TCC0.CCABUF = duty; //set compare value
	do {} while(TCC0.INTFLAGS && TC0_OVFIF_bm == 0 );  //wait
	TCC0.INTFLAGS = TC0_OVFIF_bm;
}

void send_packet(char* buffer){
	//echo back pwm duty 
	usart_send_byte(&USART_data,200);
	usart_send_byte(&USART_data,200);
	usart_send_byte(&USART_data,200);
	usart_send_byte(&USART_data,200);
	usart_send_byte(&USART_data,200);
	usart_send_int32(&USART_data,pwm_duty);
	usart_send_int32(&USART_data,10);
	usart_send_byte(&USART_data,201);
	usart_send_byte(&USART_data,201);
	usart_send_byte(&USART_data,201);
	usart_send_byte(&USART_data,201);
	usart_send_byte(&USART_data,201);
}
void parse_packet(char* buffer){
	//respond to command -- first byte is an id code for switch, next bytes carry payload
	if (buffer[0]==(char)200 && buffer[1]==(char)200 && buffer[2]==(char)200 && buffer[3]==(char)200 && buffer[4]==(char)200){
		char id = buffer[5];
		if (id==102){
			//set pwm
			pwm_duty = (uint16_t)parse_uint32(&buffer[6]);
			set_pwm(pwm_duty);
		}
	}
}
void read_usart(){
	if (USART_RXBufferData_Available(&USART_data)) {
		input_buffer[position_in_buffer] = USART_RXBuffer_GetByte(&USART_data);
		if (input_buffer[position_in_buffer] == (char)201 && 
			input_buffer[position_in_buffer-1] == (char)201 &&
			input_buffer[position_in_buffer-2] == (char)201 &&
			input_buffer[position_in_buffer-3] == (char)201 &&
			input_buffer[position_in_buffer-4] == (char)201
			){
			parse_packet(input_buffer);
			position_in_buffer = 0;
		}
		else{
			position_in_buffer++;
		}
	}
}

int main(void) {
	// set up clock
	OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
	while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock
	

	//set up usart
	PORTE.DIRSET = PIN3_bm; //TXE0
	PORTE.DIRCLR = PIN2_bm; //RXD0
	USART_InterruptDriver_Initialize(&USART_data, &USARTE0, USART_DREINTLVL_LO_gc);
	USART_Format_Set(USART_data.usart, USART_CHSIZE_8BIT_gc,
                     USART_PMODE_DISABLED_gc, 0);
	USART_RxdInterruptLevel_Set(USART_data.usart, USART_RXCINTLVL_LO_gc);
	//take f_sysclk/(BSEL+1) ~= f_baud*16 with zero scale.  See manual or spreadsheet for scale defs
	USART_Baudrate_Set(&USARTE0, 123 , -4); //230400 baud with .08% error
	USART_Rx_Enable(USART_data.usart);
	USART_Tx_Enable(USART_data.usart);
	//enable interrupts
	PMIC.CTRL |= PMIC_LOLVLEX_bm;

	//set up pwm
	PORTC.DIR |= PIN0_bm;
	TCC0.PER = 0x0400; //set up 4096 resolution
	TCC0.CTRLB = ( TCC0.CTRLB & ~TC0_WGMODE_gm ) | TC_WGMODE_SS_gc; //single slope
	TCC0.CTRLB |= TC0_CCAEN_bm & ( TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm ); //enable compare channel
	TCC0.CTRLA = ( TCC0.CTRLA & ~TC0_CLKSEL_gm ) | TC_CLKSEL_DIV1_gc;  //set clock divider
	set_pwm(pwm_duty);

	sei();

	position_in_buffer=0;
	while (1) {
		if (RTC.CNT - last_tx_rtc > 8000){ send_packet(output_buffer); last_tx_rtc = RTC.CNT;}
		read_usart();
	}
	return 0;
}

// RTC overflow interrupt service routine.
ISR(USARTE0_RXC_vect){USART_RXComplete(&USART_data);}
ISR(USARTE0_DRE_vect){USART_DataRegEmpty(&USART_data);}


