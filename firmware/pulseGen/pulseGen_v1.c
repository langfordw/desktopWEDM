//
// usart example
// output the value of a counter and echo back any serial messages received
//
// wkl 2018
//
#include <stdio.h>
#include <avr/interrupt.h>
#include "serial.h"

#define serialPort PORTC
#define txPin PIN3_bm
#define rxPin PIN2_bm

#define ledPort PORTC
#define ledPin PIN1_bm

#define chargePort PORTC
#define chPin PIN6_bm //charge
#define dchPin PIN7_bm //discharge

#define maxBufferSize 32

static char output_buffer[maxBufferSize+1] = {0};
static char input_buffer[maxBufferSize+1] = {0};

int position_in_buffer = 0;
uint16_t last_tx_rtc = 0;
volatile uint32_t counter = 0;

USART_data_t USART_data;

// send back the data
void send_packet(char* buffer, uint8_t len){
	for (uint8_t i=0; i <= len; i++) {
		usart_send_byte(&USART_data,buffer[i]);
	}
	// toggle LED for every packet sent
	// ledPort.OUT ^= ledPin;
}

// read available data and send it back
void echo_usart(){
	if (USART_RXBufferData_Available(&USART_data)) {
		input_buffer[position_in_buffer] = USART_RXBuffer_GetByte(&USART_data);
		if (input_buffer[position_in_buffer] == '\n') {
			send_packet(input_buffer,position_in_buffer);
			position_in_buffer = 0;
		}
		else{
			position_in_buffer++;
		}
	}
}

// function to write data to buffer
void writeToBuffer(char *buff, char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vsnprintf(buff, maxBufferSize, fmt, args);
    va_end(args);
}

void set_pwm(uint16_t duty){
	TCC0.CCBBUF = duty; //set compare value
	do {} while(TCC0.INTFLAGS && TC0_OVFIF_bm == 0 );  //wait
	TCC0.INTFLAGS = TC0_OVFIF_bm;
}

// interrupt service routine for RX
ISR(USARTC0_RXC_vect){
	USART_RXComplete(&USART_data);
	echo_usart();

}

// interrupt service routine for data transmit buffer empty
ISR(USARTC0_DRE_vect){
	USART_DataRegEmpty(&USART_data);
}

// ISR(RTC_OVF_vect) {
// 	counter++;
// }

int main(void) {
	// set up clock
	OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
	while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
	CCP = CCP_IOREG_gc; // enable protected register change
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

	// disable interrupts
	cli();

	//setup real time counter
	CLK.RTCCTRL	= CLK_RTCSRC_RCOSC_gc|CLK_RTCEN_bm;
	RTC.CTRL |= RTC_PRESCALER_DIV1_gc;

	// configure pins
	ledPort.DIRSET = ledPin; //led
	PORTC.OUTSET = txPin; //tx
	PORTC.DIRSET = txPin; //tx
	PORTC.DIRCLR = rxPin; //rx

	// setup USART
	USART_InterruptDriver_Initialize(&USART_data, &USARTC0, USART_DREINTLVL_LO_gc);
	USART_Format_Set(USART_data.usart, USART_CHSIZE_8BIT_gc,
                     USART_PMODE_DISABLED_gc, 0);
	USART_RxdInterruptLevel_Set(USART_data.usart, USART_RXCINTLVL_LO_gc);
	//take f_sysclk/(BSEL+1) ~= f_baud*16 with zero scale.  See manual or spreadsheet for scale defs
	USART_Baudrate_Set(&USARTC0, 131 , -3); //11520 baud with -.1% error
	USART_Rx_Enable(USART_data.usart);
	USART_Tx_Enable(USART_data.usart);

	// setup pwm
	chargePort.DIR |= ledPin;
	TCC0.PER = 0x0400; //set up 4096 resolution
	TCC0.CTRLB |= TC_WGMODE_SS_gc;//( TCC0.CTRLB & ~TC0_WGMODE_gm ) | TC_WGMODE_SS_gc; //single slope
	TCC0.CTRLB |= TC0_CCBEN_bm; //& ( TC0_CCAEN_bm | TC0_CCBEN_bm | TC0_CCCEN_bm | TC0_CCDEN_bm ); //enable compare channel
	TCC0.CTRLA |= TC_CLKSEL_DIV2_gc;  //set clock divider
	set_pwm(512);
	
	// enable interrupts
	PMIC.CTRL |= PMIC_LOLVLEX_bm;
	sei();
	
	while (1) {
		if (RTC.CNT - last_tx_rtc > 100){
			writeToBuffer(&output_buffer[0],"count=%d\n", counter++); 
			send_packet(output_buffer,maxBufferSize); 
			last_tx_rtc = RTC.CNT;
		}
	}
}


