//
// hello.blink
//
// 128a4u, blinking on PC1
//

#include <avr/io.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#define DELAY 500 // blink delay
#define LED PIN1_bm // LED bit mask

int main(void) {
   //
   // set up clock
   //
   OSC.CTRL = OSC_RC32MEN_bm; // enable 32MHz clock
   while (!(OSC.STATUS & OSC_RC32MRDY_bm)); // wait for clock to be ready
   CCP = CCP_IOREG_gc; // enable protected register change
   CLK.CTRL = CLK_SCLKSEL_RC32M_gc; // switch to 32MHz clock

   //
   // set up port
   //
   PORTC.DIR = LED;
   //
   // main loop
   //
   while (1) {
      // PORTC.OUT = LED;
      PORTC.OUTSET = LED;
      _delay_ms(DELAY);
      // PORTC.OUT = 0;
      PORTC.OUTCLR = LED;
     _delay_ms(DELAY);
   }
}
