## Past Work

Three years ago I built this:

<div align="center">

<a href="http://fab.cba.mit.edu/classes/865.15/people/will.langford/11_final/index.html" target="_blank"><img src="../images/dwedm2_01.jpg" width="750px" class="center"></a>

</div>

It looks nice… all the subsystems are there but it just hasn't been integrated.

The two biggest missing parts are: **the pulse generator**, and **the controls**. I'll go ahead and describe what I  got up to and talk through what I'd like to try this semester to get this thing going.

#### Pulse Generator

This is what we're making:

<div align="center"><video controls src="images/edm_startup.mp4" width=480px class="center"></video></div>

When I last tried this, I did manage to see some sparking:

<div align="center">
<img src="../images/sparking.gif" width="311px"> <img src="../images/sparking_result.jpg" width="350px">
</div>

My last circuit looked something like this:

<div align="center"><img src="../images/spice3.png" width="600px" class="center">

</div>

Which became a sort-of kludegy board:

<div align="center"><img src="../images/dwedm08.jpg" width="500px" class="center">

</div>